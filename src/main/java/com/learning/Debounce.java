package com.learning;

import java.util.concurrent.TimeUnit;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;

public class Debounce {

    private void sleep(long timeOut) {
        try {
            Thread.sleep(timeOut);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String args[]) {
        new Debounce().test();
    }

    private void test() {

        Flowable<Integer> flowable = Flowable.create(new FlowableOnSubscribe<Integer>() {

            @Override
            public void subscribe(FlowableEmitter<Integer> emitter) throws Exception {
                new Thread(() -> {
                    for (Integer i : new Integer[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }) {
                        System.out.println("-- Emiting... " + i);
                        emitter.onNext(i);
                        System.out.println("-- Emitted");
                        System.out.println("-- Sleeping..Zzzzz..");
                        sleep(100);
                    }
                    System.out.println("-- Completing...");
                    emitter.onComplete();
                    System.out.println("-- Completed");
                }).start();
            }
        }, BackpressureStrategy.BUFFER);

        flowable.map(x -> {
            System.out.println("-- Mapping..");
            return x * 2;
        }).debounce(200, TimeUnit.MILLISECONDS).subscribe(x -> {
            System.out.println("--------------------- Emitted value: " + x);
        });
    }
}