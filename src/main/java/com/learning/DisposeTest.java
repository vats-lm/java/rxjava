package com.learning;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

// Observalble nature:
// 1. By default, they are synchronous.
// 2. You need to create a thread to make them asynchronous.
// 3. doOnNext() operator is used to read the value passed by Observable's 'onNext' method.
// 4. map() operator is used to transform the value before passing it to the next operators in sequence or subscribe method.
// 5. subscribe() method only get call after Observable's 'onNext' call or Single's 'onSuccess' call.

// Dispose nature:
// 1. By default: Disposable disposed on i) Observables/Flowable 'onComplete' method execution completes. ii) Single 'onSuccess' execution completes.
// 2. doOnDispose() operator only works if Disposable is not disposed yet and you are manually disposing the Disposable with 'dispose()' method call.
// 3. When Disposable disposed then operators method may get called but not the 'subscribe' and 'doOnNext' method.

public final class DisposeTest {

    Disposable disposable = null;
    CompositeDisposable testCompositeDisposal = new CompositeDisposable();

    public static void main(String[] args) {
        new DisposeTest().testFlowable();
    }

    private void sleep(long timeOut) {
        try {
            Thread.sleep(timeOut);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void printDisposeMessage(String message) {
        if (disposable != null) {
            System.out.println(message + disposable.isDisposed());
        }
    }

    /**
     * When you dispose any observable then it just stop calling doOnNext and
     * subscribe lambda. It may or may not execute the code in flatMap or create
     * lamda.
     */
    private void testLoadData() {
        Disposable disposable = Observable.create(sbr -> {
            new Thread(() -> {
                for (int i = 0; i < 2; i++) {
                    sleep(100);
                    System.out.println("Debug: First: " + i);
                    sbr.onNext(true);
                }
                sbr.onComplete();
            }).start();
        }).flatMap(value -> Observable.create(sbr -> {
            for (int i = 0; i < 2; i++) {
                Thread.sleep(100);
                System.out.println("Debug: Second: " + i);
                sbr.onNext(true);
            }
            sbr.onComplete();
        })).doOnNext(value -> {
            System.out.println("Debug: doONNext");
        }).doOnDispose(() -> {
            System.out.println("Debug: doOnDispose: observable has been disposed");
        }).subscribe(val -> {
            System.out.println("Subscribe: " + val);
        });

        testCompositeDisposal.add(disposable);
        sleep(305);
        testCompositeDisposal.clear();
    }

    // 1. Single auto dispose when it emit success.
    // 2. When you create any Observable it is synchronus by default (see
    // testSingleIsAutoDisposableSync()).
    // 3. You need to create a thread to make it asynchronous.
    // 4. Subscribe just calls after emitter.onSuccess executes.
    // 5. doOnDispose only get call if you manually disposed the observable when it
    // is not already disposed.
    void testSingle() {

        // See 1.
        disposable = Single.create(emitter -> {
            new Thread(() -> {
                sleep(1000);
                printDisposeMessage("2. Is disposed in create lambda: ");
                System.out.println("---------- Before emitter.onSuccess()");
                emitter.onSuccess(1);
                printDisposeMessage("4. Is disposed after create lambda: ");
                System.out.println("---------- After emitter.onSuccess()");
            }).start();

        }).doOnDispose(() -> {
            System.out.println("Disposed"); // See 5.
        }).subscribe((x) -> {
            System.out.println("---------- Emitted value: " + x);

            // It *may be* null here. It is not null now because subscribe is not executed
            // instantly(because a call to emitter.onSuccess() method runs the subscribe
            // lambda).
            printDisposeMessage("3. Is disposed in subscribe lambda: ");
        });

        printDisposeMessage("1. Is disposed just after the subscribe (not subscribe lambda) ends: ");
        sleep(1100);
        printDisposeMessage("5. Is disposed after some time when you are sure that observable has been completed: ");
    }

    void testSingleSync() {

        // It proves that rxjava2 is synchronous until we introduce thread in 'emitter'
        // callback of create.
        disposable = Single.create(emitter -> {
            System.out.println("Enters into emiitter callback");
            Thread.sleep(3000);
            System.out.println("Completed wait");
            emitter.onSuccess(1);

        }).subscribe(x -> {
            System.out.println("Emitted value: " + x);

            // It always null here. Because it initialized after return from subscribe's
            // lambda.
            printDisposeMessage("2. ");
        });

        printDisposeMessage("1. ");
        printDisposeMessage("3. ");
    }

    // 1. Flowable auto dispose when it is completed.
    // 2. When you create any Observable it is synchronus by default (see
    // testSingleIsAutoDisposableSync() or testFlowableSync()).
    // 3. You need to create a thread to make it asynchronous.
    // 4. Subscribe just calls after emitter.onNext executes.
    void testFlowable() {
        Flowable<Integer> flowable = Flowable.create(new FlowableOnSubscribe<Integer>() {

            @Override
            public void subscribe(FlowableEmitter<Integer> emitter) throws Exception {
                new Thread(() -> {
                    for (Integer i : new Integer[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }) {
                        emitter.onNext(i);
                        sleep(100);
                    }
                    System.out.println("---------- Before emitter.onComplete()");
                    printDisposeMessage("3. Is disposed before : emitter.onComplete(): ");
                    emitter.onComplete();
                    System.out.println("---------- After emitter.onComplete()");
                    printDisposeMessage("4. Is disposed in after emitter.onComplete(): ");
                }).start();
            }
        }, BackpressureStrategy.BUFFER);

        disposable = flowable.map(x -> x * 2).subscribe(x -> {
            System.out.println("---------- Emitted value: " + x);
            printDisposeMessage("--- 2. Is disposed in subscribe lambda: ");
        });

        printDisposeMessage("1. Is disposed just after the subscribe ends: ");
        sleep(500);
        printDisposeMessage("5. Is disposed after some time when you are sure that observable has *not* been completed: ");
        sleep(1100);
        printDisposeMessage("5. Is disposed after some time when you are sure that observable has been completed: ");
    }

    // Sync observable always dispose itself.
    void testFlowableSync() {
        Flowable<Integer> flowable = Flowable.fromArray(new Integer[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });

        disposable = flowable.map(x -> x * 2).subscribe(x -> {
            System.out.println(x);
        });

        printDisposeMessage("1. Is disposed just after the subscribe ends: ");
        sleep(1100);
        printDisposeMessage("5. Is disposed after some time when you are sure that observable has been completed: ");
    }
}
